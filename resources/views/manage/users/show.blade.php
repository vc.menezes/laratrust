@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                        <h5>User Details</h5>
                </div>

                <div class="card-body d-flex justify-content-center">
                    <div class="card" style="width: 28rem;">
                        <div class="card-body">
                            <h5 class="card-title">{{ $user->name }}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $user->email }}</h6>
                            <p class="card-text">This is a very small description for this user.</p>
                            <ul>
                                @forelse ($user->roles as $role)
                                    <li><strong>{{$role->display_name}}</strong> ({{$role->description}})</li>
                                @empty
                                    <p>This user has not been assigned any roles yet</p>
                                @endforelse
                            </ul>
                            <a href="#" class="card-link">Send Email</a>
                            <a href="{{ route('users.edit', $user->id) }}" class="card-link">Edit User</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection