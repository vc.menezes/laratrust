@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                        <h5>Edit Role</h5>
                </div>

                <div class="card-body">
                    {!! Form::model($role, ['route' => ['roles.update', $role->id],
                    'method' => 'PUT']) !!}
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                @csrf
                                <fieldset>
                                    <legend>Role</legend>
                                    <div class="form-group">
                                        <label for="name">Name:</label>
                                        <input type="text" class="form-control" name="name" value="{{ $role->name }}" />
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="display_name">Display Name:</label>
                                        <input type="display_name" class="form-control" name="display_name" value="{{ $role->display_name }}" />
                                        @if ($errors->has('display_name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('display_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" name="description">{{ $role->description }}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <h3>Permissions</h3>
                                @foreach ($permissions as $permission)
                                    <br />
                                    {{ Form::checkbox('permissions[]', $permission->id, null) }}
                                    {{-- <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" /> --}}
                                    <label for="">{{ $permission->display_name }}</label>
                                @endforeach
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button class="btn btn-success">Edit Permission</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection