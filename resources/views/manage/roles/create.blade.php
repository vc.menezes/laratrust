@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                        <h5>Create Role</h5>
                </div>

                <div class="card-body">
                    <form action="{{ route('roles.store') }}" method="POST">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                @csrf
                                <fieldset>
                                    <legend>Role</legend>
                                    <input type="hidden" name="type_permission" value="simple" />
                                    <div class="form-group">
                                        <label for="name">Name:</label>
                                        <input type="text" class="form-control" name="name" />
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="display_name">Display Name:</label>
                                        <input type="text" class="form-control" name="display_name" />
                                        @if ($errors->has('display_name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('display_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" name="description"></textarea>
                                        @if ($errors->has('description'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <h3>Permissions</h3>
                                @foreach ($permissions as $permission)
                                    <br />
                                    <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" />
                                    <label for="">{{ $permission->display_name }}</label>
                                @endforeach
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button class="btn btn-success">Create Permission</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection